import axios from "axios";
const { REACT_APP_API_URL } = process.env;

const API_URL = REACT_APP_API_URL + "/auth";

const authService = {
  async signup(name, email, password) {
    const response = await axios.post(`${API_URL}/signup`, {
      name,
      email,
      password,
    });
    if (response.data.token) {
      localStorage.setItem("token", response.data.token);
    }
    return response.data;
  },

  async login(email, password) {
    const response = await axios.post(`${API_URL}/login`, { email, password });
    if (response.data.token) {
      localStorage.setItem("token", response.data.token);
    }
    console.log(response);
    return response.data;
  },

  logout() {
    localStorage.removeItem("token");
  },

  isLoggedIn() {
    return !!localStorage.getItem("token");
  },
};

export default authService;
