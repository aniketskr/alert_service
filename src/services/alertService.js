import axios from "axios";
import authService from "./authService";
const { REACT_APP_API_URL } = process.env;

const API_URL = REACT_APP_API_URL + "/api/alerts";

const alertService = {
  async getAlerts() {
    const token = authService.isLoggedIn()
      ? `Bearer ${localStorage.getItem("token")}`
      : null;
    const response = await axios.get(API_URL, {
      headers: {
        Authorization: token,
      },
    });
    return response.data;
  },

  async createAlert(alert) {
    const token = authService.isLoggedIn()
      ? `Bearer ${localStorage.getItem("token")}`
      : null;
    const response = await axios.post(API_URL, alert, {
      headers: {
        Authorization: token,
      },
    });
    return response.data;
  },

  async getAlert(alertId) {
    const token = authService.isLoggedIn()
      ? `Bearer ${localStorage.getItem("token")}`
      : null;
    const response = await axios.get(`${API_URL}/${alertId}`, {
      headers: {
        Authorization: token,
      },
    });
    return response.data;
  },

  async deliverAlert(alertId) {
    const token = authService.isLoggedIn()
      ? `Bearer ${localStorage.getItem("token")}`
      : null;
    const response = await axios.put(
      `${API_URL}/${alertId}/deliver`,
      {},
      {
        headers: {
          Authorization: token,
        },
      }
    );
    return response.data;
  },

  async escalateAlert(alertId) {
    const token = authService.isLoggedIn()
      ? `Bearer ${localStorage.getItem("token")}`
      : null;
    const response = await axios.put(
      `${API_URL}/${alertId}/escalate`,
      {},
      {
        headers: {
          Authorization: token,
        },
      }
    );
    return response.data;
  },
};

export default alertService;
