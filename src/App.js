import React from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import AlertsPage from "./pages/AlertsPage";
import LoginPage from "./pages/LoginPage";
import SignupPage from "./pages/SignupPage";
import NotFoundPage from "./pages/NotFoundPage";
import NavBar from "./components/NavBar";
import PrivateRoute from "./components/PrivateRoute";

const App = () => {
  return (
    <Router>
      <div>
        <NavBar />
        <div style={{ padding: "20px" }}>
          <Routes>
            <Route
              path="/"
              element={
                <PrivateRoute>
                  <AlertsPage />
                </PrivateRoute>
              }
            />
            <Route path="/login" element={<LoginPage />} />
            <Route path="/signup" element={<SignupPage />} />
            <Route path="*" element={<NotFoundPage />} />
          </Routes>
        </div>
      </div>
    </Router>
  );
};

export default App;
