import React from "react";
import { Navigate, useLocation } from "react-router-dom";
import authService from "../services/authService";

const PrivateRoute = ({ children }) => {
  const isLoggedIn = authService.isLoggedIn();
  const location = useLocation();

  return isLoggedIn ? (
    children
  ) : (
    <Navigate to="/login" state={{ from: location }} replace />
  );
};

export default PrivateRoute;
