import React, { useState } from "react";
import "./AlertForm.css";

const AlertForm = ({ onCreateAlert }) => {
  const [source, setSource] = useState("");
  const [type, setType] = useState("");
  const [severity, setSeverity] = useState("");
  const [message, setMessage] = useState("");
  const [recipients, setRecipients] = useState([]);

  const handleSubmit = (e) => {
    e.preventDefault();
    onCreateAlert({ source, type, severity, message, recipients });
    setSource("");
    setType("");
    setSeverity("");
    setMessage("");
    setRecipients([]);
  };

  const handleAddRecipient = () => {
    setRecipients([...recipients, { type: "", contact: "" }]);
  };

  const handleRecipientChange = (index, field, value) => {
    const updatedRecipients = [...recipients];
    updatedRecipients[index][field] = value;
    setRecipients(updatedRecipients);
  };

  return (
    <div className="alert-form ">
      <h2>Create Alert</h2>
      <form onSubmit={handleSubmit}>
        <div>
          <label htmlFor="source">Source:</label>
          <input
            type="text"
            id="source"
            value={source}
            onChange={(e) => setSource(e.target.value)}
            required
          />
        </div>
        <div>
          <label htmlFor="type">Type:</label>
          <input
            type="text"
            id="type"
            value={type}
            onChange={(e) => setType(e.target.value)}
            required
          />
        </div>
        <div>
          <label htmlFor="severity">Severity:</label>
          <select
            id="severity"
            value={severity}
            onChange={(e) => setSeverity(e.target.value)}
            required
          >
            <option value="">Select severity</option>
            <option value="low">Low</option>
            <option value="medium">Medium</option>
            <option value="high">High</option>
          </select>
        </div>
        <div>
          <label htmlFor="message">Message:</label>
          <textarea
            id="message"
            value={message}
            onChange={(e) => setMessage(e.target.value)}
            required
          />
        </div>
        <div>
          <label>Recipients:</label>
          {recipients.map((recipient, index) => (
            <div key={index}>
              <select
                value={recipient.type}
                onChange={(e) =>
                  handleRecipientChange(index, "type", e.target.value)
                }
                required
              >
                <option value="">Select type</option>
                <option value="email">Email</option>
                <option value="sms">SMS</option>
                <option value="slack">Slack</option>
                <option value="push">Push Notification</option>
              </select>
              <input
                type="text"
                value={recipient.contact}
                onChange={(e) =>
                  handleRecipientChange(index, "contact", e.target.value)
                }
                placeholder="Contact"
                required
              />
            </div>
          ))}
          <button type="button" onClick={handleAddRecipient}>
            Add Recipient
          </button>
        </div>
        <button className="btn" type="submit">
          Create Alert
        </button>
      </form>
    </div>
  );
};

export default AlertForm;
