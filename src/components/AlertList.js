import React from "react";
import "./AlertList.css";

const AlertList = ({ alerts, onDeliverAlert, onEscalateAlert }) => {
  return (
    <div className="alert-list">
      <h2>Alerts</h2>
      <table>
        <thead>
          <tr>
            <th>Source</th>
            <th>Type</th>
            <th>Severity</th>
            <th>Message</th>
            <th>Timestamp</th>
            <th>Recipients</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {alerts &&
            alerts.map((alert) => (
              <tr key={alert._id}>
                <td>{alert.source}</td>
                <td>{alert.type}</td>
                <td>{alert.severity}</td>
                <td>{alert.message}</td>
                <td>{new Date(alert.timestamp).toLocaleString()}</td>
                <td>
                  {alert.recipients &&
                    alert.recipients.map((recipient) => (
                      <span key={`${recipient.type}-${recipient.contact}`}>
                        {recipient.type}: {recipient.contact}
                        <br />
                      </span>
                    ))}
                </td>
                <td>
                  <button
                    onClick={() => onDeliverAlert(alert._id)}
                    disabled={alert.delivered}
                  >
                    {alert.delivered ? "Delivered" : "Mark as Delivered"}
                  </button>
                  <button
                    onClick={() => onEscalateAlert(alert._id)}
                    disabled={alert.escalated}
                  >
                    {alert.escalated ? "Escalated" : "Escalate"}
                  </button>
                </td>
              </tr>
            ))}
        </tbody>
      </table>
    </div>
  );
};

export default AlertList;
