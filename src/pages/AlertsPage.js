import React, { useEffect, useState } from "react";
import AlertList from "../components/AlertList";
import AlertForm from "../components/AlertForm";
import alertService from "../services/alertService";
import "./AlertsPage.css";

const AlertsPage = () => {
  const [alerts, setAlerts] = useState([]);

  useEffect(() => {
    fetchAlerts();
  }, []);

  const fetchAlerts = async () => {
    try {
      const response = await alertService.getAlerts();
      setAlerts(response);
    } catch (err) {
      console.error(err);
    }
  };

  const handleCreateAlert = async (newAlert) => {
    try {
      const response = await alertService.createAlert(newAlert);
      setAlerts([...alerts, response.data]);
    } catch (err) {
      console.error(err);
    }
  };

  const handleDeliverAlert = async (alertId) => {
    try {
      const response = await alertService.deliverAlert(alertId);
      setAlerts(
        alerts.map((alert) => (alert._id === alertId ? response : alert))
      );
    } catch (err) {
      console.error(err);
    }
  };

  const handleEscalateAlert = async (alertId) => {
    try {
      const response = await alertService.escalateAlert(alertId);
      setAlerts(
        alerts.map((alert) => (alert._id === alertId ? response : alert))
      );
    } catch (err) {
      console.error(err);
    }
  };

  return (
    <div className="alerts-page">
      <AlertForm onCreateAlert={handleCreateAlert} />
      <AlertList
        alerts={alerts}
        onDeliverAlert={handleDeliverAlert}
        onEscalateAlert={handleEscalateAlert}
      />
    </div>
  );
};

export default AlertsPage;
