const express = require("express");
const connectDB = require("./config/db");
const alertRoutes = require("./routes/alertRoutes");
const authRoutes = require("./routes/authRoutes");
const cors = require("cors");
const keycloak = require("./keycloak");
require("dotenv").config();

const app = express();
app.use(express.json());

// Secure routes with Keycloak authentication middleware
app.use(keycloak.middleware());

// Cors
app.use(
  cors({
    origin: "*",
    credentials: true,
    maxAge: 14400,
  })
);

// Connect to MongoDB
connectDB();

// Routes
app.use("/api", alertRoutes);
app.use("/auth", authRoutes);

// Secure route test
app.get("/secured", require("./middlewares/auth").protect, (req, res) => {
  res.send("This is a secured route");
});

const PORT = process.env.PORT || 5000;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
