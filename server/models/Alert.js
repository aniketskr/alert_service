const mongoose = require("mongoose");
const { v4: uuidv4 } = require("uuid");

const alertSchema = new mongoose.Schema({
  _id: { type: String, default: uuidv4 },
  source: { type: String, required: true },
  type: { type: String, required: true },
  severity: { type: String, required: true },
  message: { type: String, required: true },
  timestamp: { type: Date, default: Date.now },
  recipients: [
    {
      type: {
        type: String,
        enum: ["email", "sms", "push", "slack"],
        required: true,
      },
      contact: { type: String, required: true },
    },
  ],
  delivered: { type: Boolean, default: false },
  escalated: { type: Boolean, default: false },
  queuedAt: { type: Date, default: Date.now },
});

module.exports = mongoose.model("Alert", alertSchema);
