require("dotenv").config();
const AWS = require("aws-sdk");
const { WebClient } = require("@slack/web-api");
const slackClient = new WebClient(process.env.SLACK_BOT_TOKEN);
const fs = require("fs");
const path = require("path");

AWS.config.update({
  accessKeyId: process.env.AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
  region: process.env.AWS_REGION,
});

const ses = new AWS.SES();
const sns = new AWS.SNS();
const fcm = new AWS.Firehose({ apiVersion: "2015-08-04" });

const loadTemplate = (severity) => {
  const templatePath = path.join(
    __dirname,
    `../templates/${severity}-severity-template.html`
  );
  return fs.readFileSync(templatePath, "utf-8");
};

exports.sendEmail = async (to, subject, alert) => {
  const { source, type, severity, message } = alert;
  const template = loadTemplate(severity);
  const emailBody = template
    .replace("{{message}}", message)
    .replace("{{source}}", source)
    .replace("{{type}}", type);

  const params = {
    Destination: {
      ToAddresses: [to],
    },
    Message: {
      Body: {
        Html: {
          Charset: "UTF-8",
          Data: emailBody,
        },
      },
      Subject: {
        Charset: "UTF-8",
        Data: subject,
      },
    },
    Source: process.env.AWS_SES_FROM_EMAIL,
  };

  try {
    await ses.sendEmail(params).promise();
  } catch (error) {
    console.error(`Error sending email: ${error.message}`);
    throw error;
  }
};

exports.sendSMS = async (to, message) => {
  const params = {
    Message: message,
    PhoneNumber: to,
  };

  await sns.publish(params).promise();
};

exports.sendPushNotification = async (token, title, body) => {
  const params = {
    DeliveryStreamName: process.env.AWS_FIREHOSE_STREAM_NAME,
    Record: {
      Data: JSON.stringify({
        notification: {
          title,
          body,
        },
        to: token,
      }),
    },
  };

  await fcm.putRecord(params).promise();
};

exports.sendSlackNotification = async (recipient, subject, message) => {
  try {
    // Send a message to the specified Slack channel or user
    await slackClient.chat.postMessage({
      channel: recipient,
      text: `${subject}: ${message}`,
    });
  } catch (error) {
    console.error(`Error sending Slack notification: ${error.message}`);
    throw error;
  }
};
