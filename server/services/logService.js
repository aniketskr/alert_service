const AWS = require("aws-sdk");
require("dotenv").config();

AWS.config.update({
  accessKeyId: process.env.AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
  region: process.env.AWS_REGION,
});

const cloudwatch = new AWS.CloudWatchLogs();

const createLogStream = async (logGroupName, logStreamName) => {
  try {
    await cloudwatch
      .createLogStream({
        logGroupName: logGroupName,
        logStreamName: logStreamName,
      })
      .promise();
  } catch (error) {
    if (error.code !== "ResourceAlreadyExistsException") {
      throw error;
    }
  }
};

exports.logAlert = async (alert, status = "created") => {
  const logGroupName = process.env.AWS_CLOUDWATCH_LOG_GROUP;
  const logStreamName = `${alert.source}-${alert.type}`;

  await createLogStream(logGroupName, logStreamName);

  const params = {
    logGroupName: logGroupName,
    logStreamName: logStreamName,
    logEvents: [
      {
        timestamp: alert.timestamp.getTime(),
        message: JSON.stringify({
          status,
          alert,
        }),
      },
    ],
    sequenceToken: null,
  };

  await cloudwatch.putLogEvents(params).promise();
};
