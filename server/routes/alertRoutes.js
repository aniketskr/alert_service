const express = require("express");
const router = express.Router();
const alertController = require("../controllers/alertController");
const { authenticate } = require("../middlewares/auth");

router.post("/alerts", authenticate, alertController.createAlert);
router.get("/alerts", authenticate, alertController.getAlerts);
router.get("/alerts/:id", authenticate, alertController.getAlert);
router.put("/alerts/:id/deliver", authenticate, alertController.deliverAlert);
router.put("/alerts/:id/escalate", authenticate, alertController.escalateAlert);

module.exports = router;
