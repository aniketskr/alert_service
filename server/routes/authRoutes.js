const express = require("express");
const router = express.Router();
const authController = require("../controllers/authController");
const keycloak = require("../keycloak");

// User Signup
router.post(
  "/signup",
  keycloak.protect("realm:users:create"),
  authController.signup
);

// User Login
router.post("/login", authController.login);

module.exports = router;
