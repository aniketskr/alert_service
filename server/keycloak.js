const KeycloakConnect = require("keycloak-connect");

const keycloakConfig = {
  authServerUrl: "http://localhost:8080",
  realm: "Alert_Service",
  clientId: "Alert_Service",
  clientSecret: "W6aNaDZP29ok86EohZKbMb7uMWuPnREL",
  bearerOnly: true,
  serverUrl: "http://localhost:4000", // Replace with your server URL
};

const keycloak = new KeycloakConnect({}, keycloakConfig);

module.exports = keycloak;
