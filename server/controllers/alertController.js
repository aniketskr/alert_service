const Alert = require("../models/Alert");
const {
  sendEmail,
  sendSMS,
  sendPushNotification,
  sendSlackNotification,
} = require("../services/deliveryService");
const { sendToQueue } = require("../services/queueService");
const { logAlert } = require("../services/logService");

exports.createAlert = async (req, res) => {
  try {
    const { source, type, severity, message, recipients } = req.body;
    const alert = new Alert({ source, type, severity, message, recipients });
    await alert.save();

    // Send alert to queue
    await sendToQueue(alert);

    // Send notifications asynchronously
    for (const recipient of recipients) {
      switch (recipient.type) {
        case "email":
          sendEmail(recipient.contact, `Alert: ${type}`, alert).catch((err) => {
            console.error(`Error sending email: ${err.message}`);
          });
          break;
        case "sms":
          sendSMS(recipient.contact, message).catch((err) => {
            console.error(`Error sending SMS: ${err.message}`);
          });
          break;
        case "push":
          sendPushNotification(
            recipient.contact,
            `Alert: ${type}`,
            message
          ).catch((err) => {
            console.error(`Error sending push notification: ${err.message}`);
          });
          break;
        case "slack":
          sendSlackNotification(
            "alert-service-slack-integration",
            `Alert: ${type}`,
            message
          ).catch((err) => {
            console.error(`Error sending Slack notification: ${err.message}`);
          });
          break;
      }
    }

    // Log the alert
    await logAlert(alert);

    res.status(201).json(alert);
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};

exports.getAlerts = async (req, res) => {
  try {
    const alerts = await Alert.find();
    res.json(alerts);
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
};

exports.getAlert = async (req, res) => {
  try {
    const alert = await Alert.findById(req.params.id);
    if (!alert) {
      return res.status(404).json({ message: "Alert not found" });
    }
    res.json(alert);
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
};

exports.deliverAlert = async (req, res) => {
  try {
    const alert = await Alert.findById(req.params.id);
    if (!alert) {
      return res.status(404).json({ message: "Alert not found" });
    }
    alert.delivered = true;
    await alert.save();

    // Log the delivered alert
    await logAlert(alert, "delivered");

    res.json(alert);
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
};

exports.escalateAlert = async (req, res) => {
  try {
    const alert = await Alert.findById(req.params.id);
    if (!alert) {
      return res.status(404).json({ message: "Alert not found" });
    }
    alert.escalated = true;
    await alert.save();

    // Log the escalated alert
    await logAlert(alert, "escalated");

    res.json(alert);
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
};
