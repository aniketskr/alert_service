const keycloak = require("../keycloak");

exports.signup = async (req, res) => {
  try {
    const { name, email, password } = req.body;

    // Create a new user in Keycloak
    const user = await keycloak.grantManager.createUser({
      name,
      email,
      enabled: true,
      credentials: [{ type: "password", value: password }],
    });

    res.status(201).json({ message: "User created successfully" });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Internal server error" });
  }
};

exports.login = async (req, res) => {
  try {
    const { email, password } = req.body;

    // Authenticate user with Keycloak
    const accessToken = await keycloak.grantManager.obtainDirectly(
      email,
      password
    );

    res.json({ accessToken });
  } catch (error) {
    console.error(error);
    res.status(401).json({ message: "Invalid credentials" });
  }
};
