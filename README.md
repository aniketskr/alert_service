# Alert Service

Alert Service is a robust and scalable system designed to efficiently process and deliver alerts to designated recipients through multiple communication channels. It ensures timely notification and appropriate action in response to critical events or anomalies, making it an essential component for monitoring systems, financial platforms, security solutions, and more.

## Getting Started

### Prerequisites

- Node.js (v12 or higher)
- MongoDB (v4 or higher)
- AWS account and necessary credentials

### Installation

1. Clone the repository: 

    `https://gitlab.com/aniketskr/alert_service.git`

2. Install backend dependencies: 

    `cd alert-service/server`

    `npm install`

3. Set up environment variables:

    Create a `.env` file in the `server` directory and add the following variables:

    `MONGODB_URI=<your-mongodb-uri>`

    `JWT_SECRET=<your-jwt-secret>`

    `AWS_ACCESS_KEY_ID=<your-aws-access-key-id>`

    `AWS_SECRET_ACCESS_KEY=<your-aws-secret-access-key>`

    `AWS_REGION=<your-aws-region>`

    `AWS_SES_FROM_EMAIL=<your-aws-ses-from-email>`

    `AWS_FIREHOSE_STREAM_NAME=<your-aws-firehose-stream-name>`

    `AWS_SQS_QUEUE_URL=<your-aws-sqs-queue-url>`

    `AWS_CLOUDWATCH_LOG_GROUP=<your-aws-cloudwatch-log-group>`

    `SLACK_BOT_TOKEN=<your-slack-bot-token>`


4. Install frontend dependencies:

    `cd ..`

    `npm install`

5. Start the backend server:

    `cd ./server`

    `npm start`

6. Start the frontend development server:

    `cd ..`

    `npm start`


The backend server will start running on `http://localhost:4000`, and the frontend development server will be accessible at `http://localhost:3000`.


## Features

- **Alert Generation**: Ability to receive and process alerts from various sources (e.g., monitoring systems, financial platforms, security systems).
- **Alert Routing**: Route alerts to appropriate recipients based on predefined rules and configurations.
- **Multi-channel Delivery**: Support for delivering alerts through multiple channels (email, SMS, push notifications).
- **Alert Prioritization**: Capability to prioritize alerts based on severity levels or urgency.
- **Alert Deduplication**: Deduplicate alerts to avoid redundant notifications for the same event.
- **Alert Escalation**: Escalate unacknowledged or unresolved alerts to higher levels or alternate recipients.
- **Logging**: Maintain a comprehensive log of all alerts, their delivery status, and recipient responses.
- **Reporting and Analytics**: Generate reports and provide analytics on alert metrics, performance, and recipient engagement.
- **High Availability and Scalability**: Designed for high availability and scalability, leveraging cloud infrastructure and auto-scaling mechanisms.
- **Security**: Implement user authentication, authorization, and encryption for secure data transmission and storage.

## Technologies Used

### Frontend

- **React.js**: JavaScript library for building user interfaces.
- **React Router**: For handling client-side routing in the React application.
- **Axios**: For making HTTP requests from the frontend.

### Backend

- **Node.js**: JavaScript runtime for server-side applications.
- **Express.js**: Fast and minimalist web application framework for Node.js.
- **MongoDB**: NoSQL database for storing alert data and configurations.
- **AWS Services**:
  - **AWS SES (Simple Email Service)**: For sending email notifications.
  - **AWS SNS (Simple Notification Service)**: For sending SMS notifications.
  - **AWS Firehose (Amazon Kinesis Data Firehose)**: For sending push notifications.
  - **AWS SQS (Simple Queue Service)**: For queueing alerts.
  - **AWS CloudWatch Logs**: For logging alerts and system events.
- **JSON Web Tokens (JWT)**: For user authentication and authorization.
- **Bcrypt**: For hashing and securing user passwords.


